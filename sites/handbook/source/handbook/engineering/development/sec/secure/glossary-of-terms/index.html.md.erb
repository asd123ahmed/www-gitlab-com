---
layout: handbook-page-toc
title: Secure and Govern Glossary of Terms
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goals

The Secure Glossary of Terms aims to achieve the following:

- Promote a ubiquitous language that can be used everywhere - with customers, on issues, in Slack, in code.
- Improve the effectiveness of communication between Secure team members.
- Reduce the potential for miscommunication.
- New team members, and community contributors, can get up to speed faster, reducing the time to productivity.

## Scope

The definitions of the terms outlined in this document are provided in the specific context of the GitLab Secure
Products. Please note, therefore, that a term may have a different meaning to users outside of GitLab Secure.

## Terms

### Analyzer

Software that performs a scan. The scan analyzes an attack surface for Vulnerabilities and produces a report containing Findings. Reports adhere to the Secure Report Format.

Analyzers integrate into GitLab using a CI job. The report produced by the Analyzer is published as an artifact once the job is complete.
This is ingested by GitLab, to allow users to visualize and manage found vulnerabilities. More information can be found at [Security Scanner Integration](https://docs.gitlab.com/ee/development/integrations/secure).

Many GitLab Analyzers follow a standard approach using Docker to run a wrapped Scanner. For example, the Docker image `bandit-sast` is an Analyzer that wraps the Scanner `Bandit`.
The [Common library](https://gitlab.com/gitlab-org/security-products/analyzers/common) can be optionally used to assist building an Analyzer.

### Attack Surface

The different places in an application that are vulnerable to attack. Secure products discover and search the attack surface during scans.

Each product defines the attack surface differently, for example, SAST will use files and line numbers, while DAST uses URLs.

### CNA

[CVE](#CVE) Numbering Authorities (CNAs) are organizations from around the world that are authorized by the [Mitre Corporation](https://cve.mitre.org/) to assign [CVE](#CVE)s to vulnerabilities in products or services within their respective scope. [Gitlab is a CNA](https://about.gitlab.com/security/cve/).

### CVE

Common Vulnerabilities and Exposures (CVE®) is a list of common identifiers for publicly known cybersecurity
Vulnerabilities. The list is managed by the [Mitre Corporation](https://cve.mitre.org/).

### CVSS

The Common Vulnerability Scoring System is a free and open industry standard for assessing the severity of computer system security Vulnerabilities.

### CWE

Common Weakness Enumeration (CWE™) is a community-developed list of common software and hardware weakness types that have security ramifications.
"Weaknesses" are flaws, faults, bugs, Vulnerabilities, or other errors in software or hardware implementation, code, design, or architecture
that if left unaddressed could result in systems, networks, or hardware being vulnerable to attack. The CWE List and associated classification
taxonomy serve as a language that can be used to identify and describe these weaknesses in terms of CWEs.

### Deduplication

When a category's process deems findings to be the same, or if they are similar enough that a noise reduction is required, only one finding is kept and the other(s) are eliminated. This is an irreversible action. Deduplication is only done for findings in reports of the same category. Deduplication does not occur between reports of different categories.

### Duplicate Finding

A legitimate Finding which was found, but for some reason is reported multiple times. This can occur when different
scanners find the same Finding, or when a single scan inadvertently creates the same Finding more than once.

### False Positive

A Finding which does not actually exist but is incorrectly reported as existing.

### Feedback

Feedback provided by the user about a Finding. Types of Feedback include dismissal, creating an issue, or
creating a merge request.

### Finding

An asset that has the potential to be vulnerable, identified within a project by an Analyzer.

Assets include but are not restricted to source code, binary packages, containers, dependencies, networks, applications, and infrastructure.

### Grouping

We want to provide ways for users to view items in groups when there are multiple findings that are likely related but do not qualify for deduplication. Examples include findings that: should be evaluated together, would be fixed by the same action, or come from the same source. Grouping behavior for vulnerabilities is currently [in design](https://gitlab.com/gitlab-org/gitlab/-/issues/267588) and subject to change. It is intended as a way for users to visually organize vulnerabilities for noise reduction and more efficient workflow. Grouping is flexible and not a destructive or irreversible action like deduplication.


### Insignificant Finding

A legitimate Finding that a particular customer does not care about.

### Location Fingerprint

A Finding's Location Fingerprint is a text value that is unique for each location on the Attack Surface.
Each Secure product defines this according to their type of Attack Surface, for example, SAST will incorporate file path
and line number.

### Package managers and package types

#### Package managers

A Package manager is a system that manages your project dependencies.

The package manager provides a method to install new dependencies (also referred to as "packages"), manage where packages are stored on your file system, and offer capabilities for you to publish your own packages.

#### Package types

Each package manager, platform, type, or ecosystem has its own conventions and protocols to identify, locate, and provision software packages.

The following table is a non-exhaustive list of some of the package managers and types referenced in GitLab documentation and software tools.

<table class="package-managers-and-types">
  <thead>
    <tr>
      <th>Package Type</th>
      <th>Package Manager</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>gem</td>
      <td><a href="https://bundler.io/">bundler</a></td>
    </tr>
    <tr>
      <td>packagist</td>
      <td><a href="https://getcomposer.org/">composer</a></td>
    </tr>
    <tr>
      <td>conan</td>
      <td><a href="https://conan.io/">conan</a></td>
    </tr>
    <tr>
      <td>go</td>
      <td><a href="https://go.dev/blog/using-go-modules">go</a></td>
    </tr>
    <tr>
      <td rowspan="3">maven</td>
      <td><a href="https://gradle.org/">gradle</a></td>
    </tr>
    <tr>
      <td><a href="https://maven.apache.org/">maven</a></td>
    </tr>
    <tr>
      <td><a href="https://www.scala-sbt.org">sbt</a></td>
    </tr>
    <tr>
      <td rowspan="2">npm</td>
      <td><a href="https://www.npmjs.com">npm</a></td>
    </tr>
    <tr>
      <td><a href="https://classic.yarnpkg.com/en">yarn</a></td>
    </tr>
    <tr>
      <td>nuget</td>
      <td><a href="https://www.nuget.org/">nuget</a></td>
    </tr>
    <tr>
      <td rowspan="4">pypi</td>
      <td><a href="https://setuptools.pypa.io/en/latest/">setuptools</a></td>
    </tr>
    <tr>
      <td><a href="https://pip.pypa.io/en/stable">pip</a></td>
    </tr>
    <tr>
      <td><a href="https://pipenv.pypa.io/en/latest">Pipenv</a></td>
    </tr>
    <tr>
      <td><a href="https://python-poetry.org/">Poetry</a></td>
    </tr>
  </tbody>
</table>

### Pipeline Security Tab

A page that displays Findings discovered in the associated CI Pipeline.

### Post-filter

We will provide ways for users to specify criteria that update or modify vulnerability data based on scanner results. Post-filters can help reduce noise and automate manual tasks. Examples include flagging findings as likely False Positives and automatically resolving vulnerabilities that are no longer detected. These are not permanent actions and can be changed.

This will occur in the future, some of the related backlog items are [automatically resolve findings](https://gitlab.com/groups/gitlab-org/-/epics/7478) and [cheap scan](https://gitlab.com/gitlab-org/gitlab/-/issues/349926).

### Pre-filter

An irreversible action that is done to filter out target(s) before analysis occurs. This is usually provided to allow the user to reduce scope and noise as well as speed up the analysis. This should not be done if a record is needed as we currently do not store anything related to the skipped/excluded code or assets.

Examples: `DS_EXCLUDED_PATHS` should `Exclude files and directories from the scan based on the paths provided.`

### Primary Identifier

A Finding's Primary Identifier is a value that is unique for each Finding. The external type
and external ID of the Finding's [first identifier](https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v2.4.0-rc1/dist/sast-report-format.json#L228) combines to create the value.

Examples of Primary Identifiers include ZAP's `PluginID`, or `CVE` for Klar.
Note that the identifier must be stable; subsequent scans must return the same value for the same Finding, even if the location has slightly changed.

### Report Finding

A [Finding](#finding) that only exists in a report produced by an Analyzer and is yet to be persisted to the database.
The Report Finding becomes a [Vulnerability Finding](#vulnerability-finding) once it's imported into the database.

### Report Type / Scan Type

Describe the type of scan. This includes `container_scanning`, `dependency_scanning`, `dast`, `sast`, `api_fuzzing`, `coverage_fuzzing`, and `secret_detection`.
This list is subject to change as scanners are added.

Please use Scan Type instead of Report Type in future.

### Scanner

Software that can scan for Vulnerabilities. The resulting report of the scan is typically not in the Secure Report Format. Examples include ESLint, Klar, and ZAP.

### Secure Product

A group of features related to a specific area of application security with first-class support by GitLab.

Products include Container Scanning, Dependency Scanning, Dynamic Application Security Testing (DAST),
Secret Detection and Static Application Security Testing (SAST). Each of these products typically includes one or more
Analyzers.

### Secure Report Format

A standard report format that Secure products comply with when creating JSON reports. The format is described by a
[JSON schema](https://gitlab.com/gitlab-org/security-products/security-report-schemas).

### Security Dashboard

Provides an overview of all the Vulnerabilities for a project, group, or GitLab instance. Vulnerabilities are only created from Findings
discovered on the default branch of the project.

### Vendor

The party maintaining an Analyzer. As such, a vendor is responsible for integrating a Scanner into GitLab and keeping them compatible as they evolve. A vendor is not necessarily the author or maintainer of the Scanner as in the case of using an open core or OSS project as a base solution of an offering. For Scanners included as part of a GitLab distribution or gitlab.com subscription, the vendor will be listed as "GitLab".

### Vulnerability

A flaw that has a negative impact on the security of its environment. Vulnerabilities describe the error or weakness,
and do not describe where the error is located (see [Finding](#finding)).

Each Vulnerability maps to a unique Finding.
Plans are underway to group related Findings to the same Vulnerability to empower users with additional
workflows and improve the tracking of Findings.

### Vulnerability Finding

When a [Report Finding](#report-finding) is stored to the database, it becomes a vulnerability [Finding](#finding).

### Vulnerability Tracking

Deals with the responsibility of matching Findings across scans so that the life cycle of
a Finding can be understood. Engineers and Security Teams use this information to decide whether to merge code
changes, to see unresolved Findings and when they were introduced.

Vulnerabilities are tracked by comparing Location Fingerprint, Primary Identifier, and Report Type.

### Vulnerability Occurrence

Deprecated, see [Finding](#finding).

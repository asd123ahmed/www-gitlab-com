---
layout: handbook-page-toc
title: "Developer Evangelism"
description: "We build GitLab's technical brand with deep, meaningful conversations on engineering topics relevant to our community."

---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## <i class="fa fa-map-arrows" aria-hidden="true"></i> QuickLinks

[Request a Developer Evangelist Issue Template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request){:.btn .btn-lg .btn-purple-inv}
[CFP Issue Template](/handbook/marketing/community-relations/developer-evangelism/#social-media){:.btn .btn-lg .btn-purple-inv}
[Request Community Response support](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=community-response-plan){:.btn .btn-purple-inv .btn-lg}
[Team Workflow](/handbook/marketing/community-relations/developer-evangelism/workflow){:.btn .btn-purple-inv .btn-lg}
[Team General Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name[]=dev-evangelism){:.btn .btn-purple-inv .btn-lg}
[Team Activity Type Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/3811304?label_name[]=dev-evangelism){:.btn .btn-purple-inv .btn-lg}
[CFP Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1616902?label_name[]=CFP){:.btn .btn-purple-inv .btn-lg}

## <i class="fa fa-map-marker" aria-hidden="true"></i> Mission 

To support, grow, and engage the GitLab community through collaboration, content, and conversations.

## <i class="fa fa-map-o" aria-hidden="true"></i> Strategy
Developer relations and developer evangelism is an ever-expanding space that can seem saturated with marketing aimed at developers.  This type of developer marketing can have the inverse effect - causing developers and other technologists to be less likely to interact with companies and communities that seem disingenuous. 

For this reason, we choose to have a team with credibility from real-world engineering team experience.  Additionally, we enable others to tell **their** authentic stories - engineers, open source community members, and GitLab's diverse set of customers.  For those reasons, we chose the title of [`Evangelist`](https://www.merriam-webster.com/dictionary/evangelist) as defined as "an enthusiastic advocate."

When differentiating this approach from traditional developer relations programs or groups, we want to focus on areas that are often overlooked, including:
* DevOps
  - We want our work to speak to not only developers but all team members involved in the DevOps lifecycle to deliver working code to production: Product Managers, software engineers, designers, test engineers, security engineers, operations engineers, and SREs
* Enterprise
  - Developers and DevOps professionals in the enterprise have special constraints and needs.  Often these are glossed over with easy “throw out your architecture and use this new shiny thing” - we won’t do that, we’ll acknowledge real-world challenges, legacy code and enterprise constraints and help people solve those problems as well. When applicable, we switch roles into consulting and support.

### What fits in our strategy

When we are reviewing opportunities or requests for support, we must be able to answer yes to each of these questions to move forward with the work: 
1. Will this work support, grow, and/or engage the GitLab community? 
1. Is there a measurable impact? This can include influence on a GitLab KPI, progress on an OKR, or completion of responsibilites that are defined in the GitLab handbook. 
1. Has an issue been created with the appropriate weights and labels to define the work and assign a DRI? 

If the answer to any of the above questions is "no", our team will collaborate with each other and the requestor to do one of the following actions:
1. make adjustments so we can take on the work
1. find another team that is better suited to deliver the work
1. come to an agreement that the work should not be done

## <i class="fa fa-tasks" aria-hidden="true"></i> What we do

[Social media](/handbook/marketing/community-relations/developer-evangelism/#social-media){:.btn .btn-purple-inv}
[Content creation](/handbook/marketing/community-relations/developer-evangelism/#content-creation){:.btn .btn-purple-inv}
[Presentations and events](/handbook/marketing/community-relations/developer-evangelism/#presentations-and-events){:.btn .btn-purple-inv}
[CFPs](/handbook/marketing/community-relations/developer-evangelism/#cfps){:.btn .btn-purple-inv}
[Community engagement](/handbook/marketing/community-relations/developer-evangelism/#community-engagement){:.btn .btn-purple-inv}
[Tools](/handbook/marketing/community-relations/developer-evangelism/#projects){:.btn .btn-purple-inv}
[Release evangelism](/handbook/marketing/community-relations/developer-evangelism/#release-evangelism){:.btn .btn-purple-inv}
[Projects](/handbook/marketing/community-relations/developer-evangelism/#projects){:.btn .btn-purple-inv}
[OSS Contributions](/handbook/marketing/community-relations/developer-evangelism/#oss-contributions){:.btn .btn-purple-inv}
[Metrics](/handbook/marketing/community-relations/developer-evangelism/#metrics-collection-and-analysis){:.btn .btn-purple-inv}
[KeyHole](/handbook/marketing/community-relations/developer-evangelism/tools/keyhole/){:.btn .btn-purple-inv}

Our developer evangelism team can be summarized by the "Three Cs": 

1. **Content creation:** This is what many often think of when thinking of the traditional role of developer relations: writing blog posts, delivering technical talks, participating in podcasts or panels, and sharing ideas and thoughts on social media. 
1. **Community engagement:** Our team regularly engages with the wider GitLab community when they have questions, concerns, and feedback. This typically happens on GitLab issues, the GitLab Forum, Hacker News, Twitter, Stack Overflow, and other social media sites but also happens during in-person and virtual events. 
1. **Consulting:** Within GitLab, our team represents the voice of the community. When other teams are working on changes or decisions that will impact the community, we will educate them on our community, advocate for community interests, and work to ensure that any potential impacts to the community are clearly understood and addressed when communicating such changes. Our team also shares our knowledge of industry trends, emerging tools, social media strategy, and other skills to support our teammates in achieving their goals in alignment with GitLab's [Global Optimization](/handbook/values/#global-optimization) subvalue.

### Social media

We build our thought leadership on social media. See [Developer Evangelism on Social Media](/handbook/marketing/community-relations/developer-evangelism/social-media/) to learn more about our strategies and become an evangelist yourself.

### Content creation 

We build out content to help educate developers around best practices related to DevOps, GitLab, remote work, and other topics where we have expertise. Content includes presentations, demos, workshops, blog posts, and media engagements. 

We maintain a [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ) with our talks, workshops and community engagements.

<details>
<summary markdown='span'>
Learn about how we use tags and UTMs for tracking our work.
</summary>

#### UTMs for URL tagging and tracking

When contributing to articles on external sites, please request that the author or publisher include a [UTM code](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#utm-tracking) linking to the GitLab marketing website (i.e. `about.gitlab.com`) pages that are referenced in the article or story. 

The UTM codes to use are:

- utm_source: An identifier for the website where content is published
- utm_medium: e.g.: `social`, `presentation`, `email`, `partners`, `other` or any other medium specified in the [UTM Tracking sheet](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=3). To use any source not listed, the [Digital Marketing team](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/) needs to be informed.
- utm_campaign: `de-external-blogposts` or any other periodic campaigns.

Any UTM links created should be added to the [UTM Tracking sheet](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0) for GitLab-wide tracking and coordination. Review the [Digital Marketing handbook page](https://about.gitlab.com/handbook/marketing/demand-generation/digital-marketing/digital-marketing-management/#utms-for-url-tagging-and-tracking) for more information on UTM tracking.

#### Blog Post PostType

We write across diverse platforms, but a primary destination for our writings is the [GitLab Blog](/blog/), where all our blogposts include the `dev-evangelism` [postType](/handbook/marketing/blog/#post-type) in their [frontmatter](/handbook/marketing/blog/#frontmatter) for proper tracking.

#### Content Reuse

The Developer Evangelism team creates alot of content that can be reused for any campaigns. All contents and activities the team participates in are added to the team's [activity tracking sheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1818319548). You can search for relevant content and contact the author or the team on slack in the #dev-evangelism channel for clarification where needed.

</details>

### Presentations and events

Speaking at events is an essential path for our team to connect with the wider GitLab community and the tech community at large. We love to support to team members and members of the wider community on their presentations, too. We also help with the organization and execution of GitLab events and other tech community events.  

##### YouTube playlist

Recordings from past workshops and other team activities can be found on the [Developer Evangelism Team playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq-bYO9jCJaN45BBpzWSLAQ) on the GitLab Unfiltered channel on YouTube. 

#### CFPs

Our Developer Evangelists directly contribute to the wider community by speaking at conferences themselves. We also [support and manage responses to CFPs](/handbook/marketing/community-relations/developer-evangelism/cfps/) for team members across GitLab through our issue boards.

#### Speaker Enablement

Our team provides support to new and experienced speakers where necessary. These can range from presentation review, CFP ideation, or dry-run. We also host a monthly [Speakers Lean Coffee](/handbook/marketing/corporate-marketing/speaking-resources/#speakers-lean-coffee) to help folks prepare to speak. You can also [learn more](/handbook/marketing/community-relations/developer-evangelism/speaker-enablement/) about the different resources and activities you can benefit from or drop a message on [Slack](https://gitlab.slack.com/archives/CMELFQS4B) if you need direct support. 

#### Speakers Bureau

People who regularly speak about GitLab may be interested in joining the [GitLab Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).

### Community engagement

Part of our role is to respond to engage with and answer questions from community members. We do this organically on social media, when prompted by our social media team or other GitLab team members, and by [monitoring GitLab and other selected keywords on Hacker News](/handbook/marketing/community-relations/developer-evangelism/hacker-news/).

#### Office hours

You can join members of the Community Relations team, Fatima Sarah Khalid (Developer Evangelist) and Jamie Rachel (Evangelist Program Manager), every Wednesday on 11am PT as they host the GitLab Evangelist Program Office Hours. 

Folks can ask questions pertaining to community engagement including events, meetups, GitLab heroes, public speaking opportunities, and more. We look forward to connecting with you.  Check out our [Dev Evangelism Calendar](/handbook/marketing/community-relations/developer-evangelism/#-team-calendar) for more details.

#### Community response

Given the Developer Evangelism team's understanding of our community and broad knowledge of GitLab, we regularly engage in responding to situations that require intervention to address urgent and important concerns of our community members. We have a documented process how we [manage these situations](/handbook/marketing/community-relations/developer-evangelism/community-response/).

#### Mentoring and Coaching

The Developer Evangelism team practices are documented in public to inspire our community. We also offer [mentoring and coaching](/handbook/marketing/community-relations/developer-evangelism/mentoring-coaching/) to foster a diverse and welcoming community, share our knowledge and experience and help with career development. 

### Release evangelism

Developer Evangelists should always be prepared to promote our [monthly release](/handbook/marketing/community-relations/developer-evangelism/social-media/#release-evangelism) and engage in [community response on release days](/handbook/marketing/community-relations/developer-evangelism/hacker-news/#release-days) given the historical performance of release posts on Hacker News. 

### Tools

Our team uses different tools to grow and analyze our thought leadership, automate workflows, and improve written and presentation skills. See [Developer Evangelism Tools](/handbook/marketing/community-relations/developer-evangelism/tools/) for a list of all of those tools.

### Projects

Our team maintains many projects to help show off technical concepts, engage with communities, provide examples of using GitLab with other technologies, and automate our team processes. See [Developer Evangelism Projects](/handbook/marketing/community-relations/developer-evangelism/projects/) for a list of all of those projects.

### OSS Contributions

We actively contribute to OSS projects and share our technical expertise. You can learn more about our ideas and visions in our [OSS contributions](/handbook/marketing/community-relations/developer-evangelism/oss-contributions/) handbook page.

### Metrics Collection and Analysis

Measuring what we do is very important to understand our impact and how we are able to reach our OKRs. A key metric is the Developer Evangelists' cumulative Twitter impressions. [Learn more](/handbook/marketing/community-relations/developer-evangelism/metrics/) about the our tools, data collection and how to access the data sources for integrations.



## <i class="fa fa-users" aria-hidden="true"></i> Team members and focus areas

We are members of the [Community Relations team](/handbook/marketing/community-relations/).

| Team member |  Focus areas | Language skills | Projects | Technologies | Speaker Portfolio |
|-------------|-------------|--------------|-------------|--------------|-------------------|
| [Abubakar Siddiq Ango](/company/team/#abuango) <br/> Developer Evangelism Program Manager |  Program management, team content creation and repurpose. DevSecOps with a focus on the Cloud Native Ecosystem | English, Yoruba, Hausa | [DE Bot](/handbook/marketing/community-relations/developer-evangelism/projects/#developer-evangelism-bot), [Evangelists Dashboard](/handbook/marketing/community-relations/developer-evangelism/projects/#evangelist-dashboards) |  Kubernetes, CI/CD, Ruby, JavaScript, Rust | [Website](https://abuango.me/) | 
| [Brendan O'Leary](/company/team/#brendan) <br/> Staff Developer Evangelist | DevSecOps with a focus on the application developer perspective | English | [CodeChallenge.dev](/handbook/marketing/community-relations/developer-evangelism/projects/#codechallengedev),  [Evangelists Dashboard](/handbook/marketing/community-relations/developer-evangelism/projects/#evangelist-dashboards)  | SCM, GitOps, CI, .NET, JavaScript, NodeJS, VueJS, Ruby | [Talks](https://boleary.dev/talks/), [Portfolio](https://boleary.dev/portfolio/), [cfps.dev](https://cfps.dev/u/brendan/events) | 
| [Jamie Rachel](/company/team/#Jrachel1) <br/> Evangelist Program Manager | [Meetups](/community/meetups/), [Heroes](/community/heroes/) | English | [Beyond Code Series](/handbook/marketing/community-relations/evangelist-program/#beyond-code-series) | | | 
| [John Coghlan](/company/team/#john-coghlan) <br/> Manager, Developer Evangelism | Strategy and Planning in Developer Evangelism | English | | | [Website](https://coghlan.me/) | 
| [Fatima Sarah Khalid](/company/team/#sugaroverflow) <br/> Developer Evangelist | Community Engagement, DevSecOps | English | [Beyond Code Series](/handbook/marketing/community-relations/evangelist-program/#beyond-code-series) | CI, Verify, PHP, JavaScript | | 
| [Michael Friedrich](/company/team/#dnsmichi) <br/> Senior Developer Evangelist | DevSecOps with a focus on the SRE, Ops and Sec engineers' perspective | English, German, Austrian | [EveryoneCanContribute cafe meetup](/handbook/marketing/community-relations/developer-evangelism/projects/#everyonecancontribute-cafe), [opsindev.news newsletter](/handbook/marketing/community-relations/developer-evangelism/projects/#opsindevnews-newsletter), [o11y.love](/handbook/marketing/community-relations/developer-evangelism/projects/#o11ylove) | CI/CD, Observability, SRE, IaC, Security, Python, Go, C/C++, Rust, Ruby | [Talks](https://dnsmichi.at/talks/), [Portfolio](https://dnsmichi.at/talks/) , [cfps.dev](https://cfps.dev/u/dnsmichi/events) | 


### Stable counterparts 

Inspired by GitLab's [collaboration value](/handbook/values/#collaboration), the Developer Evangelism team has chosen to align ourselves as [stable counterparts](/handbook/leadership/#stable-counterparts) with divisions outside of Marketing. The alignment is as follows: 

- Alliances: [Abubakar Siddiq Ango](/company/team/#abuango)
- Product: [Michael Friedrich](/company/team/#dnsmichi)
- Sales: [Brendan O'Leary](/company/team/#brendan)
- Engineering: ?

As stable counterparts, Developer Evangelists are expected to actively engage with the divisions to identify collaboration opportunities and act as the primary point of contact for requests for DE support from these divisions.


## <i class="fa fa-code-fork" aria-hidden="true"></i> How we work 

### Issue tracker

We work in the open using the GitLab's [Corporate Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). We own the label `dev-evangelism` which can be applied to issues in any project under the [gitlab-com](https://gitlab.com/gitlab-com) and [gitlab-org](https://gitlab.com/gitlab-org) parent groups.

You can follow the latest our team is working on by looking at [our label-based issue board](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism) and [epics board](https://gitlab.com/groups/gitlab-com/-/epic_boards/25796?label_name[]=dev-evangelism).

### Issue labels

Using the [dev-evangelism](https://gitlab.com/groups/gitlab-com/-/labels?search=dev-evangelism) label on an issue means we are working on it or participating in the ongoing conversation. Team members are subscribed to issue/MR updates for this label.

The `dev-evangelism` label is accompanied by some labels we use to organize our work.

<i class="fas fa-info-circle" style="color: rgb(49, 112, 143)
;"></i>You only need to use the `dev-evangelism` label on issues requiring the attention of the Developer Evangelism team. Other labels are applied by the team as their state change or as contained in various issue templates.

{: .alert .alert-info}

#### General labels

| **CFP Labels** | **Description** |
| ---------- | ----------- |
| `DE-DueSoon` | This is used to monitor DE issues that are due soon |
| `DE-Peer-Review` | Feedback is needed on the issue from DE team members |
| `DE-Ops` | Used to label issues related to the Developer Evangelism `Ops in DevOps` theme |
| `DE-Dev` | Used to label issues related to the Developer Evangelism `Dev in DevOps` theme |
| `DE-k8s` | Used to label issues related to the Developer Evangelism `Kubernetes` theme |

### Issue management

The team creates issues for iteration, team discussions, and other issues for internal processes. These issues are tracked using the following labels:

| **Process Labels** | **Description** |
| -------------- | ----------- |
| `DE-Process::Open` | Process related issues that are still being discussed or worked |
| `DE-Process::Pending` | Process related issues on hold due to an external factor |
| `DE-Process::Done` | Completed Process issues |
| `DE-Process::FYI` | Issues that require no action from the team, but need to be aware of |

### Find us on Slack

GitLab team members can also reach us at any time on the [#developer-evangelism](https://app.slack.com/client/T02592416/CMELFQS4B) Slack channel where we share updates, ideas, and thoughts with each other and the wider team.

We use [dev-evangelism-updates](https://gitlab.slack.com/archives/C02TP3H62MV) for content shares and other updates that don't warrant generating noise in the larger channel. 

## <i class="fa fa-bookmark-o" aria-hidden="true"></i> Important bookmarks

- CFP
    - [Create CFP Meta Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFP-Meta)
    - [Create CFP Submission Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission)
- [Create Developer Evangelist Request Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=developer-evangelist-request)
- [Events Spreadsheet](https://docs.google.com/spreadsheets/d/1E4J4Kx7Eq8JXuh_o4RfSRbGyjj8IeVCiPRLOGslLcfU/edit#gid=0)
- [Events Calendar](#our-events-list)
- [Team Shared Drive](https://drive.google.com/drive/u/0/folders/0AEUOlCStMBC9Uk9PVA) (Internal)
- [Weekly Meeting Agenda](https://docs.google.com/document/d/1oPXtlWNDbeut-dbFDLXBCfiBJLWwbzjjK9CFu5o8QzU/edit#) (Internal)
- [Team Collab Session Agenda Doc](https://docs.google.com/document/d/1Hs2DrUf9QJQR8fSsNO9WgX7c9sS46NSjEDuKHrrvPv0/edit#heading=h.vgcz1npa9iy) (Internal)
- Team Issue Boards
    - [General](https://gitlab.com/groups/gitlab-com/-/boards/1565342?&label_name%5B%5D=dev-evangelism)
    - [CFP](https://gitlab.com/groups/gitlab-com/-/boards/1616902?&label_name%5B%5D=DE-CFP)
    - [Content](https://gitlab.com/groups/gitlab-com/-/boards/1624080?&label_name%5B%5D=dev-evangelism)
    - [Milestones](https://gitlab.com/groups/gitlab-com/-/boards/1672643?label_name%5B%5D=dev-evangelism)
- [Team Activity Tracking sheet](https://docs.google.com/spreadsheets/d/1KX8uf-4Ov8ybztJibQlGr9HvgH9VobpA8Nv5ecny1N4/edit#gid=1818319548)

### Templates

- [Action Template for Announcement Responses](https://docs.google.com/document/d/1Dhe2hFFZCDRK6eLhrfFqa-iz0bFX8gTZ4keoHo5KrtM/edit) (Internal)

## <i class="fa fa-calendar" aria-hidden="true"></i> Team Calendar

The Developer Evangelism Team calendar contains team member speaking engagements, important conferences, CFP timelines, and other important dates.

<iframe src="https://calendar.google.com/calendar/embed?src=gitlab.com_eta7o4tn4btn8h0f8eid5q98ro%40group.calendar.google.com&ctz=Europe%2FAmsterdam" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

Developer Evangelists should add new speaking engagements to this calendar as they are scheduled. The Developer Evangelist Program Manager will manage team-wide events such as industry conferences and their CFP timelines. If you need write access to this calendar, please contact a member of the Community Relations team via Slack. 

## <i class="fa fa-newspaper-o" aria-hidden="true"></i> Learn more about Developer Evangelism as a practice

A good overview with specific area definitions can be found in the [DevRel Notebook](https://github.com/konradsopala/devrel-notebook). [DevRel Resources](https://devrelresourc.es/) is a comprehensive collection of resources on DevRel.

We engage with Developer advocacy, relations and evangelism friends on social media:

- [Twitter list: Dev Avocados](https://twitter.com/i/lists/1012393598262874112/members) by Quintessence Anx (DevRel Collective founder)
- [Twitter list: DevRel](https://twitter.com/i/lists/1288789359865606145/members) by Michael
- [DevRel Contacts](https://docs.google.com/document/d/1ZX4BIwJTL0nVdkpRvLYDdk67jQfkRD_ErJWWHn-4KP8/edit) (Internal)

Our KPIs and processes follow industry best practices. We regularly iterate on new ideas and different strategies. The following articles can be helpful to explore new ways of Developer Evangelism:

- [Measuring Success and KPIs in Developer Relations - Community Contributed Outline](https://dev.to/tessamero/measuring-success-and-kpis-in-developer-relations-community-contributed-outline-1383)
- [Measuring the Impact of Your Developer Relations Team](https://openviewpartners.com/blog/measuring-the-impact-of-your-developer-relations-team/)
- [Developer evangelism and GitHub metrics - Or why stars are not the answer](https://devrel.net/strategy-and-metrics/developer-evangelism-github-metrics)
- [Developer Evangelism with Tessa Mero of Cisco](https://openchannel.io/blog/developer-evangelism-tessa-mero-cisco/)
- [What Do You Do as a Developer Advocate (🥑) at Elastic?](https://xeraa.net/blog/2020_what-do-you-do-as-a-developer-advocate-at-elastic/)
- [How to teach code](https://welearncode.com/teaching-code/) by Ali Spittel

## <i class="fa fa-external-link" aria-hidden="true"></i> Useful links

1. [GitLab Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/)
1. [How to be an evangelist](/handbook/marketing/community-relations/developer-evangelism/how-to-be-an-evangelist/)
1. [How to submit a successful conference proposal](/handbook/marketing/community-relations/developer-evangelism/writing-cfps/)
1. [Consortiums we work with](/handbook/marketing/community-relations/opensource-program/#consortium-memberships-and-sponsorships)
1. [Speaking logistics](/handbook/marketing/community-relations/developer-evangelism/speaking-logistics/)

---
layout: handbook-page-toc
title: FY21-Q4 L&D Mental Health Newsletter
---

## Thanks for reading the first edition of the GitLab mental health awareness newsletter!

Thanks for taking the time to read the first quarterly Mental Health newsletter from the GitLab Learning and Development team!

After hosting [Mental Health Awareness week](https://about.gitlab.com/blog/2020/12/21/gitlab-mental-health-awareness-week-recap/) at the end of 2020, we heard from the GitLab team that a continued conversation about mental health would help provide resources, encourage conversation, and avoid burnout long-term.

To consistently support team members in addressing signs of burnout, we hope this newsletter will

1. Highlight resources you can access as a GitLab team member to support your mental health
1. Organize external resources we think you might find helpful
1. Encourage you to have conversations about mental health, taking time off, and managing burnout with your team and colleagues
1. Empower managers to support their team members 

## Leadership feature 

This quarter, the L&D team met with Wendy Barnes, Chief People Officer at GitLab, to ask a few questions about how team members can best manage burnout in an all-remote workplace.

Here's what we asked Wendy:

1. What strategies can team members use to manage burnout?
	
1. How can managers support their team members in taking time off?

1.  How can managers help prevent burnout?


<iframe width="560" height="315" src="https://www.youtube.com/embed/oN8lzhQmFf4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## GitLab Resource Feature

[GitLab Paid Time Off Policy](/handbook/paid-time-off/)

Taking time off is important for managing burnout, and returning to work after a holiday or extended bread can feel overwhelming.

If you haven't recently reviewed our PTO policy, here are a few key points to review:

1. [A GitLab team member's guide to time off](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)
1. [Communicating your time off](/handbook/paid-time-off/#communicating-your-time-off)
1. [Covid-19 medical leave policy](/handbook/total-rewards/benefits/covid-19/#covid-19-medical-leave-policy)

It's important to remember that [rest and time off is productive](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive). In December, we welcomed John Fitch, co-author of [Time Off](https://www.timeoffbook.com/), for a live speaker series discussing how time off can be used as a tool to manage burnout.

If you haven't watched the recording yet, take some time to do so:

<iframe width="560" height="315" src="https://www.youtube.com/embed/BDvpoouM-us" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

During the interview, John touches on the following topics:

1. Examples of how taking time off can look and feel, and the positive impact this time has on our overall work performance mental health
1. How managers can support and lead team members to take time off
1. Strategies for returning from time off to make the most of the perspectives and time for reflection you've gained



## Team member resources

On January 11th, the L&D team launched access to LinkedIn Learning for GitLab team members! In the LinkedIn Learning catalog, there are lots of courses and resources you can access to learn how to manage burnout, prioritize your mental health, and care for yourself and others during stressful times. Below are a few LinkedIn Learning courses we suggest you check out! 
Suggested Course titles on LinkedIn Learning:

1. Enhancing Resilience
1. Supporting your Mental Health while Working from Home
1. Avoiding Burnout
1. Mindful Meditations for Work and Life


## Manager resources

People leaders can encourage and coach their team in taking time off to manage burnout. Here are a few strategies that manager can use to demonstrate why managing burnout is so important:

1. Lead by example. Take time off yourself, and when you're off, unplug from work
1. Use [PTO by Roots](/handbook/paid-time-off/#pto-by-roots) to communicate your time off
1. When you come back from time off, share photos and stories about what you did while you were away. Consider having a [social call](/handbook/communication/#social-call) with your team, or using a [Slack bot](/handbook/communication/#slackbots) like [GeekBot](https://geekbot.com/) to connect asynchronously. Not only can this time help demonstrate to your team the value of taking time off, but it can help [build trust](/handbook/leadership/building-trust/)
1. Help your team prepare to take time off. Use time in a 1:1 or Slack conversation to assess priorities leading up to a long time away to determine what should be the focus, what can wait, and what the rest of the team can take on. This can help your team feel less overwhelmed as they prepare to take time off and help them feel confident in truly unplugging
1. Ask questions and take time to listen to stories about your team member's time off when they return



## Discussion 

Have questions about mental health resources at GitLab? Or maybe you have a thought or resource that came up for you after reading this newsletter? Here's how to reach out:

| Question | Where to Ask |
| ----- | ----- |
| GitLab PTO policy | [#total-rewards Slack channel](https://app.slack.com/client/T02592416/CTVK60M32/thread/CETG54GQ0-1609232817.392300) |
| Learning opportunities about mental health and burnout management | [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W/thread/CETG54GQ0-1609232817.392300) |
| Ideas, contributions, and feedback about this newsletter          | [Discussion Issue](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health/-/issues/1) |



